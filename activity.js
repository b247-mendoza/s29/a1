// 1. Finding users with letter 's' in first name and 'd' in last name
db.users.find({$or: [{firstName: {$regex: 's', $options: '$i'} },{lastName: {$regex: 'd', $options: '$i'} }]},{firstName:1,lastName:1, _id : 0});

// 2. Finding users from HR department and age is greater than or equal to 70.

db.users.find({$and: [{department: "HR"},{age: {$gte:70} }] });

// 3. Finding users with letter 'e' in their firstname and has an age of less than or equal to 30.

db.users.find({$and: [{firstName: {$regex: 'e', $options: '$i'}},{age: {$lte: 30} }] });